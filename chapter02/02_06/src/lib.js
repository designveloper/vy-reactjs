import React from 'react'
import text from './title.json'

export const hello = (
	<h1 id='title' className='header'
		style={{backgroundColor: 'purple', color: 'grey'}}>
		{text.hello}
	</h1>
)

export const goodbye = (
	<h1 id='title' className='header'
		style={{backgroundColor: 'blue', color: 'orange'}}>
		{text.goodbye}
	</h1>
)
