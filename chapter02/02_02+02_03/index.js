const { createElement } = React;
const { render } = ReactDOM;

const style = {
    backgroundColor: 'orange',
    color: 'white',
    fontFamily: 'verdana'
};

render(
	//style={{backgroundColor: 'orange', color: 'white', fontFamily: 'verdana'}}
	<h1 id='title'
		className='header'
		style={style}>
	Hello World
	</h1>,
	document.getElementById('react-container')
);
