import FaShield from 'react-icons/lib/fa/shield'
import { Component, PropTypes } from 'react'

export class Member extends Component {

render() {
	const {admin, name, thumbnail, email, makeAdmin} = this.props;
    return (
        <div className="member">
			<div className="name">
				<h1>{name}
					{(admin) ? <FaShield /> : null}</h1>
			</div>
			<img src={thumbnail} alt="profile picture"/>
			<a onClick={makeAdmin}>Make ADMIN</a>
			<p><a href="{`mailto:${email}`}">{email}</a></p>
        </div>
    )
}
}

export default Member
