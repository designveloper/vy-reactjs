import { Component } from 'react'
import fetch from 'isomorphic-fetch'
import Member from './member'

class MemberList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            members: [
            {
                name: "Joe Wilson",
                email: "joe.wilson@example.com",
                thumbnail: "https://randomuser.me/api/portraits/men/53.jpg"
            },
            {
                name: "Stacy Gardner",
                email: "stacy.gardner@example.com",
                thumbnail: "https://randomuser.me/api/portraits/women/74.jpg"
            },
            {
                name: "Billy Young",
                email: "billy.young@example.com",
                thumbnail: "https://randomuser.me/api/portraits/men/34.jpg"
            }
          ]
        }
    }

    render() {
        const {members} = this.state
        return (
            <div className="member-list">
            	<h1>Society Members</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Thumbnail</th>
                        </tr>
                    </thead>
                    <tbody>
                        {members.map(
                            (data, i) =>
                                <Member key={i}
                                    onCLick={(email) => console.log(email)}
                                    {...data} />
                        )}
                    </tbody>
                </table>
            </div>
        )
   }
}

export default MemberList
