import React from 'react'
import { render } from 'react-dom'
import './stylesheets/ui.scss'
import './stylesheets/index.scss'
// import {BookReadCount} from './components/BookReadCount'
// import {BookReadList} from './components/BookReadList'
import {App} from './components/App'
import {Whoops404} from './components/Whoops404'
import {Router, Route, hashHistory} from 'react-router'

window.React = React;

render(
	//listen to browser's address bar and keep track of changes
	<Router history={hashHistory}>
		<Route path="/" component={App}/>
		<Route path="list-books" component={App}>
			<Route path=":filter" component={App} />
		</Route>
		<Route path="add-book" component={App} />
		<Route path="*" component={Whoops404}/>
	</Router>,

	// <BookReadCount />,

	document.getElementById('react-container')
)
