import {PropTypes} from 'react'
import {Autocomplete} from './Autocomplete'

const exBooks = [
    "Chaos and Harmony",
    "Harry Potter",
    "The Kite Runner",
    "Into the Wild",
    "Miracles of the Namiya General Store",
    "A Wrinkle in Time",
    "The Song of Achhilles",
    "Coraline",
    "The Letters",
    "Faust"
]

export const AddBookForm = ({title, date, entertainment, knowledge, onNewBook}) => {
    let _title, _date, _entertainment, _knowledge;

    const submit = (e) => {
        e.preventDefault();
        //sent obj
        onNewBook({
            title: _title.value,
            date: _date.value,
            entertainment: _entertainment.checked,
            knowledge: _knowledge.checked
        })
        //reset to empty input after submitting
        _title.value = ''
        _date.value = ''
        _entertainment.checked = ''
        _knowledge.checked = ''
    }

    return (
        <form onSubmit={submit} className="add-book-form">
            <label htmlFor="title">Book Title</label>
            <Autocomplete options={exBooks}
                    ref={input => _title = input}/>

            <label htmlFor="date">Date</label>
            <input id="date"
                    type="date" required
                    defaultValue={date}
                    ref={input => _date = input}/>

            <div>
                <input id="entertainment"
                        type="checkbox"
                        defaultChecked={entertainment}
                        ref={input => _entertainment = input}
                        />
                <label htmlFor="entertainment">Entertainment Book</label>
            </div>
            <div>
                <input id="knowledge"
                        type="checkbox"
                        defaultChecked={knowledge}
                        ref={input => _knowledge = input}
                        />
                <label htmlFor="knowledge">Knowledge Book</label>
            </div>
            <button>Add Book</button>
        </form>
    )
}

AddBookForm.defaultProps = {
    title: "The Little Prince",
    date: "2017-08-15",
    entertainment: true,
    knowledge: false
}


AddBookForm.propTypes = {
    title: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    entertainment: PropTypes.bool,
    knowledge: PropTypes.bool
}
