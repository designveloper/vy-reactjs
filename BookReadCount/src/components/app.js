// import {createClass} from 'react'
import {Component} from 'react'
import {BookReadList} from './BookReadList'
import {BookReadCount} from './BookReadCount'
import {AddBookForm} from './AddBookForm'
import {Menu} from './Menu'

export class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            allBookRead: [
    			{
    				title: "Cosmos",
    				date: "2017-08-05",
    				entertainment: false,
    				knowledge: true
    			},
    		]
        }
        this.addBooks = this.addBooks.bind(this)
    }

    addBooks(newBook) {
        this.setState({
            //push all existed state in new state obj, than add the new book obj
            allBookRead: [
                ...this.state.allBookRead,
                newBook
            ]
        })
    }

    countBook(filter) {
        return this.state.allBookRead.filter((book) => (
            (filter) ? book[filter] : book
        )).length
    }

    render() {
        return (
            <div className="app">
            <Menu />
            {(this.props.location.pathname === "/") ?
                <BookReadCount total={this.countBook()}
                                entertainment={
                                    this.countBook("entertainment")}
                                knowledge={
                                    this.countBook("knowledge")} /> :
                (this.props.location.pathname === "/add-book") ?
                    <AddBookForm onNewBook={this.addBooks}/> :
                    <BookReadList books={this.state.allBookRead}
                    filter={this.props.params.filter} />
            }

            </div>
        )
    }
}
