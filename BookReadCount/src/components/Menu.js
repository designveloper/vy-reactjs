import {Link} from 'react-router'
import HomeIco from 'react-icons/lib/fa/home'
import AddBookmark from 'react-icons/lib/go/bookmark'
import ListIcon from 'react-icons/lib/fa/th-list'

export const Menu = () =>
    <nav className="menu">
        <Link to="/" activeClassName="selected">
            <HomeIco />
        </Link>
        <Link to="/add-book" activeClassName="selected">
            <AddBookmark />
        </Link>
        <Link to="/list-books" activeClassName="selected">
            <ListIcon />
        </Link>
    </nav>
