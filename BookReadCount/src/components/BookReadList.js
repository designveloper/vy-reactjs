import Book from 'react-icons/lib/fa/book'
import Castle from 'react-icons/lib/fa/fort-awesome'
import Pencil from 'react-icons/lib/fa/pencil'
import {BookReadRow} from './BookReadRow'
import {PropTypes} from 'react'
import {Link} from 'react-router'

export const BookReadList = ({books, filter}) => {
    const filteredBooks = (!filter ||
            !filter.match(/entertainment|knowledge/)) ?
            books :
            books.filter(book => book[filter]) //selected prototype for object, return true or false
    return (
        <div className="book-read-list">
            <table>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Title</th>
                        <th>Entertainment</th>
                        <th>Knowledge</th>
                    </tr>
                    <tr>
                        <td colSpan={4}>
                            <Link to="/list-books">
                                All Books
                            </Link>
                            <Link to="/list-books/entertainment">
                                Entertainment Books
                            </Link>
                            <Link to="/list-books/knowledge">
                                Knowledge Books
                            </Link>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    {filteredBooks.map((book, i) =>
                        <BookReadRow key={i} //help with smart rendering
                                    {...book}
                        />
                        )}
                </tbody>
            </table>
        </div>
    )
}

BookReadList.propTypes = {
    books: function(props) {
        //check books type
        if(!Array.isArray(props.books)) {
            return new Error(
                "BookReadList should be an array"
            )
        } else if(!props.books.length) {
            return new Error(
                "BookReadList should have at least one record"
            )
        } else {
            return null
        }
    }
}
