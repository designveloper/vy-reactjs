import {Component} from 'react'

export class Autocomplete extends Component {
    get value() {
        return this.refs.inputTitle.value
    }

    set value(inputValue) {
        this.refs.inputTitle.value = inputValue
    }

    render() {
        return (
            <div>
                <input ref="inputTitle"
                type="text" list="ex-titles" />
                <datalist id="ex-titles">
                    {this.props.options.map(
                        (opt, i) =>
                        <option key={i}>{opt}</option>
                    )}
                </datalist>
            </div>
        )
    }
}
