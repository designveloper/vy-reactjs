import '../stylesheets/ui.scss'
import Book from 'react-icons/lib/fa/book'
import Castle from 'react-icons/lib/fa/fort-awesome'
import Pencil from 'react-icons/lib/fa/pencil'
import Calendar from 'react-icons/lib/fa/calendar'
import {PropTypes} from 'react'

const percentToDecimal = (decimal => ((decimal*100) + '%'));

const calcGoalProgress = ((total, goal) => percentToDecimal(total/goal));

//can also use const BookReadCount = React.createClass
//can also use class BookReadCount extends React.Component
export const BookReadCount = ({total=25, entertainment=15, knowledge=7, goal=30}) => (
    <div className="book-read-count">
        <div className="total-books">
            <span>{total}</span>
                <Book />
            <span>books</span>
        </div>
        <div className="entertainment-books">
            <span>{entertainment}</span>
                <Castle />
            <span>books</span>
        </div>
        <div className="knowledge-books">
            <span>{knowledge}</span>
            <Pencil />
            <span>books</span>
        </div>
        <div>
            <span>
                {calcGoalProgress(
                    total,
                    goal)}
            </span>
        </div>
    </div>
)

BookReadCount.propTypes = {
    total: PropTypes.number,
    entertainment: PropTypes.number,
    knowledge: PropTypes.number,
    goal: PropTypes.number
}
