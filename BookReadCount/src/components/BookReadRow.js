import Book from 'react-icons/lib/fa/book'
import Castle from 'react-icons/lib/fa/fort-awesome'
import Pencil from 'react-icons/lib/fa/pencil'
import {PropTypes} from 'react'

export const BookReadRow = ({title, date, entertainment, knowledge}) => (
    <tr>
        <td>
            {date}
        </td>
        <td>
            {title}
        </td>
        <td>
            {(entertainment) ? <Castle /> : null}
        </td>
        <td>
            {(knowledge) ? <Pencil /> : null}
        </td>
    </tr>
)

BookReadRow.propTypes = {
    title: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    entertainment: PropTypes.bool,
    knowledge: PropTypes.bool
}
