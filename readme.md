# DSV - ReactJS

## Introduction:
* React is:
    * A library for user interfaces
    * Created at fb and ins
    * React Native for mobile
* React [documentation](https://facebook.github.io/react/docs)
* Tools: react-detector & React Developer Tools
* **DOM diffing**: make updating the DOM faster
    * (happen when) Compare rendered content with the new UI changes (that are about to be made)
    * make only the minimal changes necessary
    * compare JS obj
    * faster than writing/reading from DOM

## Intro to JSX and Babel
* #### Pure React:
    * `React.createElement(type, property, child content)`: example view Code.01
    * `ReactDOM.render(element, where-to-render)`
* #### JSX and Babel transpiling
    * **JSX**: JS as XML, a tag based syntax we can use to create react element
    * [cdn](https://cdnjs.com)
        * babel-core (ver 5.8 for in-browser transpile)
        * import ReactJS file with the type `text/babel`
    * Babel static transpiling
        * `babel-cli` (install using *npm*)
        * set up all the presets or everything we want to transpile using Babel (and remember to npm install them as well!) - the example use `babel-preset-latest`, `babel-preset-react`, `babel-preset-stage-0`
        * command: `babel [src-file] --out-file [dest-file]`
* #### Webpack
    * **Webpack**: bundle all files into  1 JS file, making fewer HTTP requests
        * is a module bundler
        * create static files
        * create automates processes
    * create a `webpack.config.js` to describe everything we want Webpack to do with our file
    * install:
        * `webpack` (example use ver. 1.13.3)
        * `babel-loader` (example use ver. 6.2.5)
        * `webpack-dev-server` (example use ver 1.16.2): for live reload
        * `react` and `react-dom`
        * `json-loader`: load json file
        * `style-loader`, `css-loader`, `autoprefix-loader`: load css file and add suitable prefix for it
        * `node-sass`: load scss file
    * we can export and import html element/JSX represents object to be used in other obj... (remember to import b4 use)
    * webpack can be used to transpile scss to css

## React Components
### For the sake of God, the first letter of a Component MUST BE UPPERCASE
* purpose of a component is to display data
* think React as an obj and every property is a key
* Component syntax:
    * `createClass` (React)
    * ES6 class (extends React.Component)
    * Stateless functional component: functions take in property info and return JSX elements
        * `const component = (props) => ()`
        * no constructor no state no this props as arg (?)
        * better in performance with component has child function (solution: separate the child func and pass it as an arg.)
        * not support life cycle (use HOCs or `recompose` paackage to "fix" this)
    * **Higher Order Components**:  takes a component, return a new component. Pass a stateless component to a stateful (class) component as props
* React [icon](http://gorangajic.github.io/react-icons/go.html)
* Default property
* React.PropTypes [doc](https://facebook.github.io/react/docs/typechecking-with-proptypes.html)
* **State**:
    * *State* represent the possible conditions of your app
        * indentify the minimal reprentation of app state
        * reduce state to as few components as possible
        * avoid overwriting state variables
    * `getInitialState` in a `createClass` component is how we initialize state as default
    * in ES6 `extends Component`, we use `constructor` in place of `getInitialState`

## Using the React Router
* React Router [link](https://github.com/ReactTraining/react-router)
* Route history: listen to the browser's address bar
* routing example: `<Route path="/" component={pathName}`
* get the path: `this.props.location.pathname`
* link in react-router: `<Link to 'path'>`
* route can be nested in route. Check Code.02

## Form and refs
* The [htmlFor](https://www.w3schools.com/jsref/prop_label_htmlfor.asp) property sets or returns the value of the for attribute of a label.
* use `.preventDefault` to prevent default submit
* `refs` is used to return a reference to element. [More](https://facebook.github.io/react/docs/refs-and-the-dom.html "Refs and the DOM") about `refs`
* in stateless function, we have to use this: `ref={input => _title = input}`
* we can use `datalist` to make autocomplete component. It's a [HTML5 feature](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/datalist "MDN reference")

## The Component Lifecycle
* `<a href="mailto:${email}"`: open default mail program, create new message. More [here](https://css-tricks.com/snippets/html/mailto-links/ "Mailto Links")
* React Component Lifecycle:
    * [react component](https://facebook.github.io/react/docs/react-component.html)
    * [understanding the React Component Lifecycle](http://busypeoples.github.io/post/react-component-lifecycle/)
* fetch: `'isomorphic-fetch'`
* Mounting and updating cycle:
    * `componentWillMount`
    * `componentDidMount`
    * `componentWillUpdate`
    * `shouldComponentUpdate`: to avoid rendering when it is not necessary, to choose the specific component to update



## Code Reference
### Code.01
```javascript
const title = React.createElement(
    'h1',
    {id: 'title', className: 'header'},
    'Hello World'
    )
```
### Code.02
```javascript
<Route path="list-books" component={App}>
    <Route path=":filter" component={App} />
</Route>
```
